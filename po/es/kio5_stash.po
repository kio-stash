# Spanish translations for kio5_stash.po package.
# Copyright (C) 2017 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2017.
# Eloy Cuadra <ecuadra@eloihr.net>, 2017, 2022.
# Sofia Priego <spriego@darksylvania.net>, 2017.
# Sofía Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: kio5_stash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-03 00:43+0000\n"
"PO-Revision-Date: 2022-11-26 18:07+0100\n"
"Last-Translator: Sofía Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.11.80\n"

#: kioworker/filestash.cpp:205
#, kde-format
msgid "The file either does not exist or has not been stashed yet."
msgstr "El archivo no existe o todavía no se ha añadido al almacén temporal."

#: kioworker/filestash.cpp:213
#, kde-format
msgid "The UDS Entry could not be created."
msgstr "No se ha podido crear la entrada UDS."

#: kioworker/filestash.cpp:230
#, kde-format
msgid "Could not create a directory"
msgstr "No se ha podido crear un directorio"

#: kioworker/filestash.cpp:327 kioworker/filestash.cpp:333
#: kioworker/filestash.cpp:339
#, kde-format
msgid "Could not copy."
msgstr "No se ha podido copiar."

#: kioworker/filestash.cpp:342
#, kde-format
msgid "Copying to mtp workers is still under development!"
msgstr "La copia en módulos de trabajo mtp está todavía en desarrollo."

#: kioworker/filestash.cpp:387 kioworker/filestash.cpp:393
#: kioworker/filestash.cpp:401
#, kde-format
msgid "Could not rename."
msgstr "No se ha podido cambiar el nombre."

#~ msgid "Could not stat."
#~ msgstr "No se ha podido hacer «stat»."
